using UnityEngine;
[CreateAssetMenu]
public class BuildingData : ScriptableObject
{
    public string nameBuilding;
    public GameObject readyBuilding;
    public GameObject foundationBuilding;
    public GameObject destroedBulding;
    public float baseBuildPrice;
    public float baseRepairPrice;
    public float baseSellPrice;
    public float baseLevelUpPrice;    
    

    public GameObject GetVisionByState(StateBuilding stateBuilding)
    {
        GameObject curentVision=null;

        switch (stateBuilding)
        {
            case StateBuilding.foundationState:
                curentVision = foundationBuilding; 
                break;
            case StateBuilding.activeState:
                curentVision = readyBuilding;
                break;
            case StateBuilding.destroyedState:
                curentVision = destroedBulding;
                break;           
        }
        return curentVision;
    }
}
