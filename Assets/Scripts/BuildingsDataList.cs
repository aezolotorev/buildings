using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class BuildingsDataList : ScriptableObject
{
   public List<BaseBuilding> buildings;
}
