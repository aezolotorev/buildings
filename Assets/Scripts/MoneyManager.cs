using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UniRx;

public class MoneyManager : MonoBehaviour, ISaverLoader
{
    public static MoneyManager Instance;
    public TextMeshProUGUI textMoney;
    private ReactiveProperty<int> money = new ReactiveProperty<int>(1000);


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        money.Subscribe(newMoney => textMoney.text = newMoney.ToString());
    }
    [ContextMenu("ADD 10000")]
    public void Add1000()
    {
        AddMOney(1000);
    }


    public bool IsEnoughMoney(int price)
    {
        if (money.Value >= price) return true;
        else return false;
    }

    public void AddMOney(int amount)
    {
        money.Value += amount;
    }

    public void EarnMoney(int price)
    {
        money .Value-= price; 
    }

    public void Save()
    {
        SaveData.SetInt("money", money.Value);
    }

    public void Load()
    {
        money.Value = SaveData.GetInt("money", 1000);
    }

    public void DeleteSave()
    {
        SaveData.Delete("money");
        money.Value = 1000;
    }
}
