﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public abstract class BaseBuilding : MonoBehaviour
{
    public static Action<BaseBuilding> onUpgrade, onSell, onBuy, onRepair;
    public static Action<BaseBuilding> onChangeStatus;
    public StateBuilding stateBuilding { get; private set; }
    public GameObject currentVisual;
    public BuildingData buildingData;
    public int level { get; private set; }

    private bool wasChecked=false;

    public void Setup(int level, StateBuilding stateBuilding)
    {
        this.level = level;
        this.stateBuilding = stateBuilding;
        if (currentVisual != null)
        {
            Destroy(currentVisual);
        }
        currentVisual = Instantiate(buildingData.GetVisionByState(stateBuilding), this.transform);
    }

    private void CheckNeighbours()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other != null)
        {
            if (other.CompareTag("Building")&&!wasChecked)
            {
                wasChecked = true;
                BaseBuilding neighbour = other.GetComponent<BaseBuilding>();
                neighbour.MakeDestroyed();
                MakeDestroyed();
            }
        }
    }

    public void MakeDestroyed()
    {
        Setup(1, StateBuilding.destroyedState);
    }

    public virtual void Sell()
    {
        onSell?.Invoke(this);
        MoneyManager.Instance.AddMOney((int)buildingData.baseSellPrice);
        Destroy(this.gameObject);       
        
    }
    public virtual void Upgrade()
    {
        if (MoneyManager.Instance.IsEnoughMoney((int)buildingData.baseLevelUpPrice))
        {
            MoneyManager.Instance.EarnMoney((int)buildingData.baseLevelUpPrice);
            level++;
            Debug.Log("Потрачено" + buildingData.baseLevelUpPrice + " на апгрейд, уровень здания стал " + level);
            onUpgrade?.Invoke(this);            
        }
        else
        {
            Debug.Log("Не хватает денюжек");
        }
    }
    public virtual void Buy()
    {

        if (MoneyManager.Instance.IsEnoughMoney((int)buildingData.baseBuildPrice))
        {
            MoneyManager.Instance.EarnMoney((int)buildingData.baseBuildPrice);
            Debug.Log("Потрачено" + buildingData.baseBuildPrice + " на покупку,  здания");
            onBuy?.Invoke(this);
            stateBuilding = StateBuilding.activeState;
            CreateVisualByState(stateBuilding);
            onBuy?.Invoke(this);
        }
        else
        {
            Debug.Log("Не хватает денюжек");
        }



    }

    public virtual void Repair()
    {

        if (MoneyManager.Instance.IsEnoughMoney((int)buildingData.baseRepairPrice))
        {
            MoneyManager.Instance.EarnMoney((int)buildingData.baseRepairPrice);
            Debug.Log("Потрачено" + buildingData.baseRepairPrice + " на ремонт");
            onRepair?.Invoke(this);
            stateBuilding = StateBuilding.activeState;
            CreateVisualByState(stateBuilding);
        }
        else
        {
            Debug.Log("Не хватает денюжек");
        }
    }






    private void CreateVisualByState(StateBuilding stateBuilding)
    {
        if (currentVisual != null)
        {
            Destroy(currentVisual);
        }
        currentVisual = Instantiate(buildingData.GetVisionByState(stateBuilding), this.transform);
    }


    public float GetSellPrice()
    {
        return buildingData.baseSellPrice;
    }
}

public enum StateBuilding
{
    activeState, destroyedState, foundationState
}
