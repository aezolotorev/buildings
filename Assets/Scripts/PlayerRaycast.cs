﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerRaycast : MonoBehaviour
{
    public static Action <BaseBuilding> onClickOnBuildings;
    public static Action <Vector3> onClickOnPlane;

    public Camera mainCamera;
    public float zoomSpeed = 2500f;
    public float minZoomDistance = 50f;
    public float maxZoomDistance = 200f;

    private void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {        
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject()) return;
            Vector3 clickPosition = Input.mousePosition;           
            Ray ray = Camera.main.ScreenPointToRay(clickPosition);            
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                BaseBuilding baseBuilding = hit.collider.GetComponent<BaseBuilding>();  
                if(baseBuilding != null) { 
                    onClickOnBuildings(baseBuilding);
                }
                if (hit.collider.CompareTag("Plane"))
                {
                    Debug.Log(hit.point);
                    onClickOnPlane?.Invoke(hit.point);
                }
            }
           
        }


        float scrollDelta = Input.mouseScrollDelta.y;

        if (scrollDelta != 0)
        {
            Vector3 forwardDirection = mainCamera.transform.forward;

           
            float zoomDelta = scrollDelta * zoomSpeed * Time.deltaTime;
            float newZoom = Mathf.Clamp(mainCamera.transform.localPosition.z - zoomDelta, -maxZoomDistance, -minZoomDistance);
            
            Vector3 newPosition = mainCamera.transform.localPosition + forwardDirection * zoomDelta;
            
            newPosition.y = Mathf.Clamp(newPosition.y, 0f, Mathf.Infinity);

            mainCamera.transform.localPosition = newPosition;
        }
    }
}
