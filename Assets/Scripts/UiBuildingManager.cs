using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class UiBuildingManager : MonoBehaviour
{
    public static Action<Vector3, string> onBuild;
    public static Action<BaseBuilding> onDestroyBuild;
    [SerializeField] private GameObject buildingPanel;
    [SerializeField] private GameObject startBuildPanel;
    [SerializeField] private TextMeshProUGUI buildingInfo;
    private BaseBuilding currentBuilding;

    [SerializeField] private Button upgradeButton, sellButton, repairButton, buyButton;
    [SerializeField] private Button closeButton;
    private Vector3 pointForBuild;

    private void Start()
    {
        PlayerRaycast.onClickOnBuildings += ClickOnBuilding;
        PlayerRaycast.onClickOnPlane += ClickOnPlane;
        BuldButtons.onClick += TryBuildNow;
        BaseBuilding.onSell += WasSell;
        BaseBuilding.onBuy += WasBuyed;
        BaseBuilding.onRepair += WasRepaired;
        BaseBuilding.onUpgrade += WasUpgraded;
        InitButtons();
    }
    private void OnDestroy()
    {
        PlayerRaycast.onClickOnBuildings -= ClickOnBuilding;
        PlayerRaycast.onClickOnPlane = ClickOnPlane;
        BuldButtons.onClick -= TryBuildNow;
        BaseBuilding.onSell -= WasSell;
        BaseBuilding.onBuy -= WasBuyed;
        BaseBuilding.onRepair -= WasUpgraded;
    }


    private void WasSell(BaseBuilding baseBuilding)
    {
        Close();
    }

    private void WasBuyed(BaseBuilding baseBuilding)
    {
        ClickOnBuilding(baseBuilding);
    }

    private void WasRepaired(BaseBuilding baseBuilding)
    {
        ClickOnBuilding(baseBuilding);
    }

    private void WasUpgraded(BaseBuilding baseBuilding)
    {
        ClickOnBuilding(baseBuilding);
    }

    private void InitButtons()
    {
        upgradeButton.onClick.AddListener(Upgrade);
        sellButton.onClick.AddListener(Sell);
        repairButton.onClick.AddListener(Repair);
        buyButton.onClick.AddListener(Buy);
        closeButton.onClick.AddListener(Close);
    }

    private void ClickOnBuilding(BaseBuilding baseBuilding)
    {
        closeButton.gameObject.SetActive(true);
        startBuildPanel.SetActive(false);
        buildingPanel.SetActive(true);
        buildingInfo.gameObject.SetActive(true);
        buildingInfo.text = baseBuilding.buildingData.name+" level"+ baseBuilding.level +" "+baseBuilding.stateBuilding.ToString()+"build Price" + baseBuilding.buildingData.baseBuildPrice+"etc.";
        HideButtons();
        currentBuilding = baseBuilding;
        switch (currentBuilding.stateBuilding)
        {
            case StateBuilding.foundationState:
                ShowFoundationPanel();
                break;
            case StateBuilding.activeState:
                ShowActivePanel();
                break;
            case StateBuilding.destroyedState:
                ShowDestroydPanel();
                break;
        }
    }

    private void ShowFoundationPanel()
    {
        buyButton.gameObject.SetActive(true);
    }
    private void ShowActivePanel()
    {
        upgradeButton.gameObject.SetActive(true);
        sellButton.gameObject.SetActive(true);
    }
    private void ShowDestroydPanel()
    {
        repairButton.gameObject.SetActive(true);
        sellButton.gameObject.SetActive(true);
    }

    private void HideButtons()
    {
        upgradeButton.gameObject.SetActive(false);
        buyButton.gameObject.SetActive(false);
        repairButton.gameObject.SetActive(false);
        sellButton.gameObject.SetActive(false);
    }


    private void Sell()
    {
        if (currentBuilding != null)
        {
            onDestroyBuild?.Invoke(currentBuilding);
            currentBuilding.Sell();
        }
    }
    private void Buy()
    {
        if (currentBuilding != null)
        {
            currentBuilding.Buy();
        }
    }
    private void Upgrade()
    {
        if (currentBuilding != null)
        {
            currentBuilding.Upgrade();
        }
    }
    private void Repair()
    {
        if (currentBuilding != null)
        {
            currentBuilding.Repair();
        }
    }

    private void Close()
    {
        currentBuilding = null;
        buildingPanel.SetActive(false);
        startBuildPanel.SetActive(false);
        closeButton.gameObject.SetActive(false);
    }

    private void ClickOnPlane(Vector3 pos)
    {
        buildingInfo.gameObject.SetActive(false);
        pointForBuild = pos;
        buildingPanel.SetActive(false);
        startBuildPanel.SetActive(true);
        closeButton.gameObject.SetActive(true);
    }

    private void TryBuildNow(string nameBuild)
    {
        onBuild?.Invoke(pointForBuild, nameBuild);
    }

}


