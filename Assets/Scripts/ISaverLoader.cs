using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISaverLoader 
{   
    void Save();
    void Load();
    void DeleteSave(); 
}
