﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadManager : MonoBehaviour
{
    [SerializeField] private List<ISaverLoader> listOfSaverLoaders = new();



    private void Awake()
    {
        FindAllSaverLoaders();
    }

    private void Start()
    {
        CheckSaves();
    }

    private void CheckSaves()
    {
        if (SaveData.IsHasProgress())
        {
            Debug.Log("Есть соххранеие");
            Load();
        }
    }
    [ContextMenu("Save")]
    public void Save()
    {
        SaveData.SetInt("progress", 1);
        foreach (var loader in listOfSaverLoaders) { loader.Save(); }
    }
    [ContextMenu("Load")]
    public void Load()
    {
        foreach (var loader in listOfSaverLoaders) { loader.Load(); }
    }
    [ContextMenu("Delete")]
    public void Delete()
    {
        SaveData.DeleteProgress();
        foreach (var loader in listOfSaverLoaders) { loader.DeleteSave(); }
    }


    private void FindAllSaverLoaders()
    {
        
        ISaverLoader[] saverLoaderComponents = GetComponents<ISaverLoader>();
        
        foreach (var saverLoaderComponent in saverLoaderComponents)
        {
            if (!listOfSaverLoaders.Contains(saverLoaderComponent))
            {
                listOfSaverLoaders.Add(saverLoaderComponent);
            }
        }
    }
}
