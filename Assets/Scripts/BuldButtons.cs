using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuldButtons : MonoBehaviour
{
    public static Action<string> onClick;
    [SerializeField] private Button button;
    [SerializeField] private string nameBuild;
    [SerializeField] private TextMeshProUGUI nameText;


    private void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(Click);
        nameText.text = nameBuild;
    }

    private void Click()
    {
        onClick?.Invoke(nameBuild);
    }

    public string GetNameBuild()
    {
        return nameBuild;
    }


}
