using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class BuildingManager : MonoBehaviour, ISaverLoader
{
    [SerializeField] private BuildingsDataList buildingsList;
    [SerializeField] private List<BaseBuilding> buildingsOnScene=new List<BaseBuilding>();


    private void Start()
    {
        UiBuildingManager.onBuild += TryBuild;
        UiBuildingManager.onDestroyBuild += TryRemove;
    }
    private void OnDestroy()
    {
        UiBuildingManager.onBuild -= TryBuild;
        UiBuildingManager.onDestroyBuild -=TryRemove;
    }

    private void TryBuild(Vector3 pos, string nameBuild)
    {
        BaseBuilding curBuilding = Instantiate(GetBuildingByName(nameBuild), pos, Quaternion.identity);      
        curBuilding.Setup(1, StateBuilding.foundationState);
        buildingsOnScene.Add(curBuilding);
    }

   
    private void TryRemove(BaseBuilding curBuilding)
    {
        if (buildingsOnScene.Contains(curBuilding))
        {
            buildingsOnScene.Remove(curBuilding);
        }
    }


    private void ClearBuildings()
    {
        if (buildingsOnScene.Count > 0)
        {
            foreach (var v in buildingsOnScene)
            {
                Destroy(v.gameObject);
            }
            buildingsOnScene.Clear();
        }
    }



    public void Load()
    {
        ClearBuildings();

        if (SaveData.Has("buildingsOnScene"))
        {
            string xml = SaveData.GetString("buildingsOnScene");
            if (xml.Length > 0)
            {                

                XmlSerializer serializer = new XmlSerializer(typeof(List<BuildingsOnSceneData>));
                using (StringReader reader = new StringReader(xml))
                {
                    List<BuildingsOnSceneData> buildingsList = (List<BuildingsOnSceneData>)serializer.Deserialize(reader);

                    foreach (var b in buildingsList)
                    {
                        BaseBuilding buildingOnScene = GetBuildingByName(b.nameBuilding);

                        if (buildingOnScene != null)
                        {
                            BaseBuilding curBuilding = Instantiate(buildingOnScene);
                            curBuilding.transform.position = b.GetPosition();
                            curBuilding.Setup(b.levelBuilding, b.stateBuilding);
                            buildingsOnScene.Add(curBuilding);
                        }
                    }
                }
            }
        }
    }
    public void Save()
    {
        List<BuildingsOnSceneData> saveList = new List<BuildingsOnSceneData>();

        foreach (var b in buildingsOnScene)
        {
            BuildingsOnSceneData buildingSaveData = new BuildingsOnSceneData(b);
            saveList.Add(buildingSaveData);
        }

        XmlSerializer serializer = new XmlSerializer(typeof(List<BuildingsOnSceneData>));
        using (StringWriter writer = new StringWriter())
        {
            serializer.Serialize(writer, saveList);
            string xml = writer.ToString();
            Debug.Log(xml);
            SaveData.SetString("buildingsOnScene", xml);
        }
    }
    public void DeleteSave()
    {
        ClearBuildings();
        SaveData.Delete("buildingsOnScene");
    }

    private BaseBuilding GetBuildingByName(string name)
    {
        BaseBuilding building = null;
        foreach (var b in buildingsList.buildings)
        {
            if (b.name.ToLower() == name.ToLower())
            {
                 building= b;
            }
        }
        return building;

    }
}

[System.Serializable]
public class BuildingsOnSceneData
{
    public string nameBuilding;
    public float posX;
    public float posY;
    public float posZ;    
    public int levelBuilding;
    public StateBuilding stateBuilding;
    public BuildingsOnSceneData()
    {
    }
    public BuildingsOnSceneData(BaseBuilding baseBuilding)
    {
        nameBuilding = baseBuilding.buildingData.nameBuilding;       
        posX = (float)Math.Round(baseBuilding.transform.position.x, 3);
        posY = (float)Math.Round(baseBuilding.transform.position.y, 3);
        posZ = (float)Math.Round(baseBuilding.transform.position.z, 3);
        levelBuilding = baseBuilding.level;
        stateBuilding = baseBuilding.stateBuilding;
    }

    public Vector3 GetPosition()
    {
        return new Vector3(posX, posY, posZ);
    }    
}